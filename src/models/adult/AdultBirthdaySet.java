package models.adult;

import models.BirthdaySet;
import models.Cake;
import models.Card;
import models.Gift;

public class AdultBirthdaySet implements BirthdaySet {

    public Gift getGift() {
        return new AdultGift();
    }

    public Card getCard() {
        return new AdultCard();
    }

    public Cake getCake() {
        return new AdultCake();
    }
}
