package models.adult;

import models.Gift;

public class AdultGift implements Gift {

    @Override
    public String toString() {
        return "Поздравительный подарок";
    }
}
