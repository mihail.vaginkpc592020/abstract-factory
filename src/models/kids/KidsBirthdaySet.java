package models.kids;

import models.BirthdaySet;
import models.Cake;
import models.Card;
import models.Gift;

public class KidsBirthdaySet implements BirthdaySet {

    public Gift getGift() {
        return new KidsGift();
    }

    public Card getCard() {
        return new KidsCard();
    }

    public Cake getCake() {
        return new KidsCake();
    }
}
