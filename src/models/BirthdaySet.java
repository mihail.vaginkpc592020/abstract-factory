package models;

public interface BirthdaySet {

    Gift getGift();

    Card getCard();

    Cake getCake();
}
