import models.BirthdaySet;
import models.adult.AdultBirthdaySet;
import models.kids.KidsBirthdaySet;

public class Main {
    public static void main(String[] args) {
        BirthdaySet adultBirthdaySet = new AdultBirthdaySet();
        BirthdaySet kidsBirthdaySet = new KidsBirthdaySet();
        System.out.println(doParty(adultBirthdaySet));
        System.out.println(doParty(kidsBirthdaySet));
    }

    public static String doParty(BirthdaySet birthdaySet) {
        return birthdaySet.getCard() + " и " + birthdaySet.getCake() + " и " + birthdaySet.getGift();
    }
}